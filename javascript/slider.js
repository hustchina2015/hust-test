// JavaScript Document


	//类锚点  回到顶部  EDIT.SIMBA.2015.08.14
	$.fn.anchor = function(opts){
		opts = $.extend({
			tapType   	     :  "click",     	//默认的点击方式---click,touchend
			speed     	     :  500, 			//回到顶部的速度
			minHeight 	     :  0,			    //回到顶部按钮出现的最小高度
			forID            :  null
		},opts || {});
		var ths = this;//解决this指向问题
		ths.on(opts.tapType,function(){
			var h = 0;
			if(opts.forID == null || opts.forID == ""){
				h = 0;
			}else{
				h = $("#" + opts.forID).getTop();
			}

			$(this).siblings().removeClass("choo");
			$(this).addClass("choo");
			
			$('html,body').animate({scrollTop:h},opts.speed);
		});

	    //为窗口的scroll事件绑定处理函数
	    $(window).scroll(function(){
	        //获取窗口的滚动条的垂直位置
	        var s = $(window).scrollTop();
	        //当窗口的滚动条的垂直位置大于页面的最小高度时，让返回顶部元素渐现，否则渐隐
	        if( s >= opts.minHeight){
	            ths.fadeIn(100);
	        }else{
	            ths.fadeOut(200);
	        };
	    });
	}
	}